# Moths -- Birds Predatation

<img src="http://netlogoweb.org/assets/images/desktopicon.png" alt="Drawing" width="100" height="100""/>

## Modification de la librairie moths de Netlogo.


Le modèle original montre les papillons de nuit volant en cercle autour d'une
lumière en suivant des règles de vol. Nous avons ajouté à ce modèle
un prédateur (un oiseau) dans son environnement. 
Le prédateur possède des propriétés de vol similaire à celui des papillons 
de nuit mais il peut mourir s'il ne mange pas les papillons de nuit 
(si option birds-god-mode activé ) et il ne peut pas trop s'approcher 
des lumières contrairement aux papillons
de nuit qui eux sont attirés par les lumières. 

------------

### Changelogs

>**v0.1 :**

> - ajout number-birds : slider permettant de choisir le nombre d'oiseau
> - ajout bird-gaine-from-food : slider permettant de choir le nombre 
    de points de vie de l'oiseau à chaque papillon mangé
> - ajout birds-god-mode? : switch permettant d'avoir des oiseaux 
    invincibles ou non
> - ajout de compteur birds b et moths m : nombre de b et de m à linstant t
> - ajout d'un graphe de population
> - ajout d'un switch light? : possibilité de supprimer les 
lumières pour voir le comportement des agents

--------------

### Modèle utilisé :

Wolf-sheep predation, Ants, Ant Lines, Fireflies, Flocking


--------------

## Screenshot

<img src="Images/app_screenshot.png" alt="Drawing" width="60%"/>
